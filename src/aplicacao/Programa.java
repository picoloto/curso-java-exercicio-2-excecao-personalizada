package aplicacao;

import java.util.InputMismatchException;
import java.util.Scanner;

import modelo.entidades.Conta;
import modelo.excecoes.DominioExcecao;

public class Programa {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		try {
			System.out.println("Dados da conta");
			System.out.print("N�mero: ");
			int numero = sc.nextInt();
			sc.nextLine();

			System.out.print("Titular: ");
			String titular = sc.nextLine();

			System.out.print("Saldo inicial: ");
			double saldo = sc.nextDouble();

			System.out.print("Limite para Saque: ");
			double limite = sc.nextDouble();

			Conta conta = new Conta(numero, titular, saldo, limite);

			System.out.println();
			System.out.print("Valor do saque: ");
			double saque = sc.nextDouble();
			conta.saque(saque);
			System.out.print("Novo saldo: " + String.format("%.2f", conta.getSaldo()));

			System.out.println();
			System.out.println();
			System.out.print("Valor do deposito: ");
			double deposito = sc.nextDouble();
			conta.deposito(deposito);
			System.out.print("Novo saldo: " + String.format("%.2f", conta.getSaldo()));
		} catch (InputMismatchException e) {
			System.out.println("Dado digitado invalido");
		} catch (DominioExcecao e) {
			System.out.println(e.getMessage());
		}

		sc.close();

	}

}
