package modelo.entidades;

import modelo.excecoes.DominioExcecao;

public class Conta {
	
	private Integer numero;
	private String titular;
	private Double saldo;
	private Double limite;
	
	public Conta() {
	}
	
	public Conta(Integer numero, String titular, Double saldo, Double limite) throws DominioExcecao {
		if (limite > saldo) {
			throw new DominioExcecao("Limite n�o pode ser maior que saldo inicial!");
		} 
		
		this.numero = numero;
		this.titular = titular;
		this.saldo = saldo;
		this.limite = limite;
	}

	public Integer getNumero() {
		return numero;
	}

	public void setNumero(Integer numero) {
		this.numero = numero;
	}

	public String getTitular() {
		return titular;
	}

	public void setTitular(String titular) {
		this.titular = titular;
	}

	public Double getSaldo() {
		return saldo;
	}

	public Double getLimite() {
		return limite;
	}

	public void setLimite(Double limite) {
		this.limite = limite;
	}
	
	public void deposito(double quantia) {	
		saldo += quantia;
	}
	
	public void saque(double quantia) throws DominioExcecao {
		if (quantia > limite) {
			throw new DominioExcecao("A quantia informada � maior que o limite para saques!");
		} 
		if (quantia > saldo){
			throw new DominioExcecao("A quantia informada � maior que o saldo!");
		}
		
		saldo -= quantia;
	}

}
